module libproxy;

version(Windows):
private:
import core.sys.windows.windows : LoadLibraryA, GetProcAddress, VirtualProtect;
import std.file : tempDir, write, exists, mkdirRecurse;
import std.path : buildPath, dirName;
import std.process : thisProcessID;
import std.conv : to;
import std.string : toStringz;
enum lib = import("../resources/liboriginal.dll");

static assert(lib[0..2] == "MZ", "Invalid DOS header");
enum pPEBase = read!(uint, 0x3C);

static assert(lib[pPEBase .. pPEBase + 4] == "PE\x00\x00", "Invalid PE header");
enum export_va = read!(uint, pPEBase + 0x78);
enum section_count = read!(ushort, pPEBase + 0x06);
enum section_align = read!(uint, pPEBase + 0x38);
static foreach(section; 0 .. section_count){
	mixin("enum vs"~section.stringof~" = read!(uint, pPEBase + 0xF8 + 0x28 * section + 0x08);");
	mixin("enum va"~section.stringof~" = read!(uint, pPEBase + 0xF8 + 0x28 * section + 0x0C);");
	static if (mixin("isRVAInSection(va"~section.stringof~", vs"~section.stringof~", export_va, section_align)")){
		enum praw = read!(uint, pPEBase + 0xF8 + 0x28 * section + 0x14);
		mixin("enum pExport=RVA2Offset(va"~section.stringof~",vs"~section.stringof~", export_va, section_align, praw);");
		mixin("enum pName=RVA2Offset(va"~section.stringof~",vs"~section.stringof~", read!(uint, pExport + 0x0C), section_align, praw);");
		enum count_names = read!(uint, pExport + 0x18);
		mixin("enum pFuncNames=RVA2Offset(va"~section.stringof~",vs"~section.stringof~", read!(uint, pExport + 0x20), section_align, praw);");
		static foreach(i; 0 .. count_names){
			mixin("enum ppFuncName"~i.stringof~" = read!(uint, pFuncNames + i * 4);");
			mixin("enum pFuncName"~i.stringof~"=RVA2Offset(va"~section.stringof~",vs"~section.stringof~",ppFuncName"~i.stringof~", section_align, praw);");
			mixin("export pragma(mangle, readStringz!(pFuncName"~i.stringof~", 256)) extern(C) void _f"~i.stringof~"(){asm{naked;nop;nop;nop;nop;nop;}}");
		}
		public void InitLibProxy(string original = ""){
			__gshared static void* hlib;
			if (hlib !is null)
				return;

			if (!original.length){
				original = tempDir.buildPath(thisProcessID.to!string~"/"~readStringz(pName, 256));
				if(!original.dirName.exists)
					original.dirName.mkdirRecurse;
				if (!original.exists)
					original.write(lib);
			}
			hlib = LoadLibraryA(original.toStringz);

			uint vp;
			void* pFunc, pOrigFunc;
			static foreach(i; 0 .. count_names){
				mixin("enum ppFuncName"~i.stringof~" = read!(uint, pFuncNames + i * 4);");
				mixin("enum pFuncName"~i.stringof~"=RVA2Offset(va"~section.stringof~",vs"~section.stringof~",ppFuncName"~i.stringof~", section_align, praw);");
				mixin("pFunc=&_f"~i.stringof~";");
				pOrigFunc = GetProcAddress(hlib, mixin("readStringz!(pFuncName"~i.stringof~", 256)").toStringz);
				VirtualProtect(pFunc, 5, 0x40, &vp);
				*cast(byte*)pFunc = cast(byte)0xE9; // jmp
				*cast(uint*)(cast(uint)pFunc + 1) = cast(uint)pOrigFunc - (cast(uint)pFunc + 5);
				VirtualProtect(pFunc, 5, vp, &vp);
			}
		}
		pragma(msg, "\n\n\n\n\nPlease use target name \""~readStringz!(pName,256)~"\" for result library!\n\n");
	}
}

T read(T, uint offset)()
{
	T result;
	static foreach(b; 0 .. T.sizeof)
		result |= lib[offset + b] << (b * 8);
	return result;
}
string readStringz(uint offset, uint maxLength = 0xFFFF)(){
	uint length;
	static foreach(i; 0 .. maxLength)
		if (lib[offset + i] == 0 && !length)
			length = i;
	return lib[offset..offset+length];
}
bool isRVAInSection(uint va, uint vs, uint rva, int /* from Optional */ salign)
{
	uint start = va;
	uint end = start + ((vs & (salign-1))?(vs & ~(salign-1))+salign:vs);

	if(rva >= start && rva < end)
		return true;
	return false;
}
uint RVA2Offset(uint va, uint vs, uint rva, int /* from Optional */ salign, uint praw){
	if (!isRVAInSection(va, vs, rva, salign))
		return 0;
	return rva - va + praw;
}
